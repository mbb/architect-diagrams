# Tools, Libraries and Assets for Draw.io

This directory contains tools, libraries and assets that you can use to create diagrams that comply with Red Hat's color and style guide using [Draw.io](https://www.draw.io).

## Tools

[TO BE PROVIDED]

## Libraries

Custom made libraries for Draw.io can be found [here](./libraries).

* Core Colors Library (`rh-core-color.xml`) has square shapes that correspond to core colors defined in the Red Hat style guide.

![core colors](./images/rh-core-colors.png)

* Secondary Colors A Library (`rh-secondary-colors-a.xml`) has square shapes that correspond to first series of secondar colors defined in the Red Hat style guide.

![secondary colors](./images/rh-secondary-colors-a.png)

### Installing a library in your browser instance of Draw.io

Take a look at the following instructions to learn how to add a custom library to a browser instance of Draw.io.

**Step 1:** Select `Open Library...` from the `File` menu.

![Add library step `](./images/add-library-01b.jpg)

You will be presented with a dialog that presents a number of locations from which you can add a library's `xml` file. In this case you're going to select `GitLab...` to open a library from this repository.

**Step 2:** Navigate to the `xml` file that represents a library in the respository's `libraries` directory. In this example, we're interested in the file, `rh-core-color.xml`

![Add library step `](./images/add-library-02b.jpg)

**Step 3:** When you select a library's `xml` file, upon opening, it will appear in left side diagram shapes pane in the Draw.io browser window.

![Add library step `](./images/add-library-03b.jpg)

**Step 4:** Once a library appears in the shapes pane, you can drag a shape into the drawing canvas to us it.

![Add library step `](./images/add-library-04b.jpg)

### Installing a library in a desktop instance of Draw.io

Take a look at the following instructions to learn how to add a custom library to a desktop instance of Draw.io.

**Step 1:** Clone this repository to your local machine.

**Step 2:** Select `Open Library...` from the `File` menu.

![Add library step `](./images/add-library-01.jpg)

You will be presented with a dialog that will allow you to traverse the file system of your local machine.

**Step 3:** Go to the location on your computer where you cloned this repository and select a library `xml` of internest. In this example, we're interested in the file, `rh-core-color.xml`

![Add library step `](./images/add-library-02.jpg)

**Step 3:** When you select a library's `xml` file, upon opening, it will appear in left side diagram shapes pane in the Draw.io window.

![Add library step `](./images/add-library-03.jpg)

**Step 4:** Once a library appears in the shapes pane, you can drag a shape into the drawing canvas to us it.

![Add library step `](./images/add-library-04.jpg)

## Assets

[TO BE PROVIDED]